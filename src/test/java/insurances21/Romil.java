package insurances21;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Romil {
    public static void main(String[] args) throws InterruptedException {

        System.setProperty("webdriver.chrome.driver","src/test/java/chromedriver_win32/chromedriver.exe");
        WebDriver driver=new ChromeDriver();
//        System.setProperty("webdriver.gecko.driver","src/test/java/geckodriver-v0.30.0-win64/geckodriver.exe");
//        WebDriver driver=new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get("http://demo.guru99.com/test/delete_customer.php");
        WebElement element1=driver.findElement(By.xpath("/html/body/form/table/tbody/tr[2]/td[2]/input"));
        element1.sendKeys("35799765");
        WebElement element4=driver.findElement(By.name("submit"));
        Thread.sleep(5000);
        element4.click();
        Alert alert1=driver.switchTo().alert();
        String st1=driver.switchTo().alert().getText();
        System.out.println(st1);
        Thread.sleep(5000);
        alert1.accept();
        System.out.println("First alert selected");
        Alert alert2=driver.switchTo().alert();
        Thread.sleep(5000);
        alert2.accept();
        System.out.println("Last alert selected");

    }
}
