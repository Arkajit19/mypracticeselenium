package insurances21;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

public class Guru99site {
    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver","src/test/java/chromedriver_win32/chromedriver.exe");
        String base_url="http://demo.guru99.com/test/newtours/";

        WebDriver driver=new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(base_url);

        WebElement link_home=driver.findElement(By.linkText("Home"));
        WebElement td_home=driver.findElement(By.xpath("//html/body/div[2]/table/tbody/tr/td[1]/table/tbody/tr/td/table/tbody/tr/td/table/tbody"));
        Actions builder=new Actions(driver);
        Action mouseOverHome=builder.moveToElement(link_home).build();
        String bgcolor=td_home.getCssValue("background-color");
        System.out.println("Before mousehover color : "+bgcolor);
        mouseOverHome.perform();
        bgcolor=td_home.getCssValue("background-color");
        System.out.println("After mousehover color : "+bgcolor);
        Thread.sleep(7000);
        driver.close();

    }
}
