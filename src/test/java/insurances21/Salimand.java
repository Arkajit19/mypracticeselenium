package insurances21;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Salimand {

	String base_url="http://demo.guru99.com/test/newtours/";
	WebDriver driver;
	String driverlocation="C:\\Users\\arkaj\\Desktop\\codingworld\\mypracticeselenium\\src\\test\\java\\chromedriver_win32\\chromedriver.exe";
	
	@BeforeTest
	public void browserlaunch() {
		
		System.setProperty("webdriver.chrome.driver", driverlocation);
		ChromeOptions options=new ChromeOptions();
		options.addArguments("headless");
		driver=new ChromeDriver(options);
		driver.get(base_url);
	}
	
	@Test
	public void webtitlevalidate() {
		
		String expectedtitle="Welcome: Mercury Tours";
		String actualtitle=driver.getTitle();
		Assert.assertEquals(expectedtitle, actualtitle);
		
	}
	
	@AfterTest
	public void terminatebrowser() throws InterruptedException {
		
		Thread.sleep(5000);
		driver.close();
		
	}
}
