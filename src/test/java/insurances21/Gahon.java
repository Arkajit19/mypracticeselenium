package insurances21;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.Test;

public class Gahon {
  
	String base_url="http://demo.guru99.com/test/newtours/";
	String driverpath="C:\\Users\\arkaj\\Desktop\\codingworld\\mypracticeselenium\\src\\test\\java\\chromedriver_win32\\chromedriver.exe";
	WebDriver driver;
	
	@Test
	public void work() throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver",driverpath);
		ChromeOptions options=new ChromeOptions();
		options.addArguments("headless");
		driver=new ChromeDriver(options);
		driver.manage().window().maximize();
		driver.get(base_url);
		String expectedtitle="Welcome: Mercury Tours";
		Thread.sleep(5000);
		String actualtitle=driver.getTitle();
		Assert.assertEquals(expectedtitle, actualtitle);
		Thread.sleep(7000);
		driver.close();
		
	}
}
