package insurances21;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Base {
    public static void main(String[] args) throws InterruptedException {

        System.setProperty("webdriver.chrome.driver","src/test/java/chromedriver_win32/chromedriver.exe");
        WebDriver driver=new ChromeDriver();
        JavascriptExecutor js1= (JavascriptExecutor)driver;
        driver.manage().window().maximize();
        String base_url="http://demo.guru99.com/test/newtours/";
        driver.get(base_url);
        Thread.sleep(5000);
        js1.executeScript("scrollBy(0,4500)");
        WebElement element2=driver.findElement(By.xpath("//table/tbody/tr/td[2]"+
                                                            "/table/tbody/tr[4]/td"
                                                            +"/table/tbody/tr/td[2]"
                                                            +"/table/tbody/tr[2]/td[1]"+
                                                            "/table[2]/tbody/tr[3]/td[2]/font"));
        Thread.sleep(5000);
       String str7= element2.getText();
        System.out.println(str7);
        Thread.sleep(8000);
        driver.close();
    }
}
