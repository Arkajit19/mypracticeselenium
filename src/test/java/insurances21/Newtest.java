package insurances21;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Newtest {
    public static void main(String[] args) throws InterruptedException {

        System.setProperty("webdriver.chrome.driver","src/test/java/chromedriver_win32/chromedriver.exe");
        WebDriver driver=new ChromeDriver();
        driver.get("http://demo.guru99.com/test/radio.html");

        WebElement radio1=driver.findElement(By.id("vfb-7-1"));
        WebElement radio2=driver.findElement(By.id("vfb-7-2"));
        radio1.click();
        System.out.println("Option 1 one selected");
        Thread.sleep(7000);
        radio2.click();
        System.out.println("Option 2 is selected");
        Thread.sleep(5000);
        WebElement option1=driver.findElement(By.id("vfb-6-0"));
        option1.click();
        if (option1.isSelected()){

            System.out.println("The checkbox is toggled");
        }
        else {

            System.out.println("The checkbox is not toggled");
        }
    }
}
