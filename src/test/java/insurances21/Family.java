package insurances21;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Family {

    public static void main(String[] args) throws InterruptedException {

        System.setProperty("webdriver.chrome.driver","src/test/java/chromedriver_win32/chromedriver.exe");
        WebDriver driver=new ChromeDriver();
        driver.manage().window().maximize();
        JavascriptExecutor js3=(JavascriptExecutor)driver;
        String base_url="http://demo.guru99.com/test/web-table-element.php";
        driver.get(base_url);
        //js3.executeScript("scrollBy(0,1500)");
        Thread.sleep(5000);
        WebElement row4first= driver.findElement(By.xpath("//*[@id=\"leftcontainer\"]/table/tbody/tr[4]/td[1]"));
        WebElement row4text= driver.findElement(By.xpath("//*[@id=\"leftcontainer\"]/table/tbody/tr[4]/td[3]"));
        String str8=row4first.getText();
        String str7=row4text.getText();
        Thread.sleep(7000);
        System.out.println("The first cell value is : "+str8);
        System.out.println("The cell value is : "+str7);
        Thread.sleep(9000);
        driver.close();


    }
}
