package insurances21;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.List;

public class Pushan {

    public static void main(String[] args) throws InterruptedException, ParseException {

        System.setProperty("webdriver.chrome.driver","src/test/java/chromedriver_win32/chromedriver.exe");
        ChromeOptions options=new ChromeOptions();
        options.addArguments("headless");
        WebDriver driver;
        driver=new ChromeDriver(options);
        driver.manage().window().maximize();
        String base_url="http://demo.guru99.com/test/web-table-element.php";
        driver.get(base_url);
        double m=0,r=0;
        String max;
        Thread.sleep(3000);
        List<WebElement>col1=driver.findElements(By.xpath("//*[@id=\"leftcontainer\"]/table/thead/tr/th"));
        int k1= col1.size();
        Thread.sleep(5000);
        System.out.println("Number of colomn : "+k1);
        List<WebElement>row1=driver.findElements(By.xpath("//*[@id=\"leftcontainer\"]/table/tbody/tr"));
        int l1= row1.size();
        System.out.println("Number of row : "+l1);

        for (int a1=0;a1< row1.size();a1++){

            max=driver.findElement(By.xpath("//*[@id=\"leftcontainer\"]/table/tbody/tr[" +(a1+1)+ "]/td[3]")).getText();
            Thread.sleep(6000);
            NumberFormat f=NumberFormat.getNumberInstance();
            Number num1=f.parse(max);
            max=num1.toString();
            m=Double.parseDouble(max);
            if (m>r){

                r=m;
            }
        }
        Thread.sleep(3000);
        System.out.println("The maximum number is : "+r);
//        Thread.sleep(9000);
//        driver.close();
    }
}
